/* const consultaTablaFijo = async (telfijo) => {

    let req = {};
    let params = telfijo;
    req.class = "radiusAPI";
    req.method = "estadoFijoPruebas";
    req.params = params;
    const datos = JSON.stringify(req);

    return await axios({
        method: "post",
        url: "https://192.168.98.26/proyectos/includes/interfazajax.inc.php",
        data: datos,
        processData: false,
        dataType: "json",
    });

}

const consultaTablaRadius = async (pcode, IUA) => {

    //añadimos / en los pcode donde es necesario
    const iua_2 = IUA.slice(0, 1)
    if (iua_2 === "Z") {
        pcode = `${pcode}/`
    }

    let req = {};
    let params = pcode;
    req.class = "radiusAPI";
    req.method = "consultaRadius";
    req.params = params;
    const datos = JSON.stringify(req);

    return await axios({
        method: "post",
        url: "https://192.168.98.26/proyectos/includes/interfazajax.inc.php",
        data: datos,
        processData: false,
        dataType: "json",
    }).then((result) => {
        return result.data.data;
        //return res  datos[i].AcctStatusType

    }).catch((err) => {

    });;


} */
/* 
async function contruirTable(provcode,IUA,fijo){


  var tabla = `{panel:title=*RADIUS*|borderStyle=dashed|borderColor=#ccc|titleBGColor=#ccc}`
  var comentarioAgente = "{panel:title=*COMENTARIO*}";

  let datos = await consultaTablaRadius(provcode+'/',IUA);
  //let datos = await consultaTablaRadius('v1815002#1_1029_58/', '318029201186');
  console.log(datos);

  //|${fecha_inicio}|${fecha_fin}|${duration}|${natIpAddress}|${ipAddress}|${bras}|${download}|${status}|

  for (let i = 0; i < datos.length; i++) {
      var fecha_inicio = datos[i].fecha;
      var fecha_fin = datos[i].fecha_fin;
      var duration = datos[i].duration;
      var ipAddress = datos[i].FramedAddress;
      var natIpAddress = datos[i].NatIpAddress;
      var bras = datos[i].Host;
      var download = datos[i].download;
      var upload = datos[i].upload;
      var status = datos[i].AcctStatusType;
      
      tabla = `|${fecha_inicio}|${fecha_fin}|${duration}|${natIpAddress}|${ipAddress}|${bras}|${download}|${upload}|${status}|{panel}\n`;
  }

  let tabaf = '{panel:title=*ESTADO FIJO*|borderStyle=dashed|borderColor=#ccc|titleBGColor=#ccc}|||Estado Xena|||ID Propietario|||PORTADO|||KENRUTADOR|||IMS|||CPE REGISTRADO|||ENUM CONFIGURADO';
  // let _consultaFijoRadius = await consultaTablaFijo('958439516');
  let _consultaFijoRadius = await consultaTablaFijo(fijo);
  console.log(_consultaFijoRadius);
  const datosFijoRadius = _consultaFijoRadius.data.estado;
  
  const XenaStatus = datosFijoRadius.XenaStaus;
  const ProprietaryID = datosFijoRadius.ProprietaryID;
  const Portability = datosFijoRadius.Portability;
  const ExistsKenrutador = datosFijoRadius.ExistsKEnrutator;
  const Route = datosFijoRadius.Route;
  const ExistsIms = datosFijoRadius.ExistsIMS;
  const CpeRegistered = datosFijoRadius.CpeRegistered;
  const ENUM = datosFijoRadius.ENUM;
  tabaf += `|${XenaStatus}|${ProprietaryID}|${Portability}|${ExistsKenrutador} : ${Route} |${ExistsIms}|${CpeRegistered}|${ENUM}{panel}`
  let tablacsims = '{panel:title=*CONFIGURACION ACS-IMS*|borderStyle=dashed|borderColor=#ccc|titleBGColor=#ADC3EC|bgColor=#FEFDFC}||';
  const ProvCode = datosFijoRadius.ProvCode;
  const SipUsernameACS = datosFijoRadius.SipUsernameACS;
  const SipUsernameIMS = datosFijoRadius.SipUsernameIMS;
  const SipPasswordACS = datosFijoRadius.SipPasswordACS;
  const SipPasswordIMS = datosFijoRadius.SipPasswordIMS;
  const PrimaryOutboundACS = datosFijoRadius.PrimaryOutboundACS;
  const PrimaryOutboundIMS = datosFijoRadius.PrimaryOutboundIMS;
  const SecondaryOutboundACS = datosFijoRadius.SecondaryOutboundACS;
  const SecondaryOutboundIMS = datosFijoRadius.SecondaryOutboundIMS;
  const RegistrarServerACS = datosFijoRadius.RegistrarServerACS;
  const RegistrarServerIMS = datosFijoRadius.RegistrarServerIMS;
  const OperatorACS = datosFijoRadius.OperatorACS;
  const OperatorIMS = datosFijoRadius.OperatorIMS;
  tablacsims += `|||Configuración ACS ||Configuración IMS |||ProvCode|${ProvCode}|${ProvCode} |||SipUsername|${SipUsernameACS}|${SipUsernameIMS}|||SipPassword|${SipPasswordACS}|${SipPasswordIMS}|||PrimaryOutboundACS|${PrimaryOutboundACS}|${PrimaryOutboundIMS}|||SecondaryOutbound|${SecondaryOutboundACS}|${SecondaryOutboundIMS}|||RegistrarServer|${RegistrarServerACS}|${RegistrarServerIMS}|||Operator|${OperatorACS}|${OperatorIMS}{panel}`;
 return tabla+tabaf+tablacsims;
  
}
 */







/* const asingUser = (averia, rel, token, user, comentario) => {

    comentario = { body: comentario };
  

    $.ajax({
      async: true,
      type: "POST",
      url: `https://jira.masmovil.com/rest/api/2/issue/${averia}/comment`,
      data: JSON.stringify(comentario),
      headers: {
        "content-Type": "application/json",
        "Accept-Language": "es-ES,es;q=0.9",
        "X-Requested-With": "XMLHttpRequest"
      }
    });
  
    var data = `inline=true&decorator=dialog&id=${rel}&assignee=${user}&comment=&commentLevel=&atl_token=${token}`
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://jira.masmovil.com/secure/AssignIssue.jspa");
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    xhr.send(data);
    console.log(`${averia} asignada para revision a : ${user}`);
  }



 */






const revisar_averias = async () => {
    var token = $("#atlassian-token").attr('content');
    sessionStorage.setItem("tokenJira", token);
    const usuario = $('meta[name=ajs-remote-user]').attr('content');
    //olt_potencia_rx.slice
    
    
    
let nav = document.querySelector('#header > nav > div > div.aui-header-primary > ul');    
      
     
      
var btn = document.createElement('button');

btn.textContent = 'hola';
btn.classList.add("EnviarData");

nav.appendChild(btn);


//envia peticion para ejecutar el python
console.log('antes');
    //document.querySelector("#header > nav > div > div.aui-header-primary > ul > button.EnviarData").addEventListener('click',function(){
        let datos = await  contruirTable('v2811001#2_1008_82/','428026865541','919328360');
        console.log(datos);
         asingUser('19798865','MAS-18504021','BN7U-SK2I-YO3N-CHP1|44738167c576dc2b97ec912519076f72858e3dda|lin','bboo.zelenza', 'ESCALADO-');
         addComentIssue('MAS-18504021','ESCALADOxx');

    //})




}

revisar_averias();